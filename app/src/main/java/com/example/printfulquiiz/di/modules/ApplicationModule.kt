package com.example.printfulquiiz.di.modules
import com.example.printfulquiiz.data.di.DataModule
import android.app.Application
import android.content.Context
import com.example.printfulquiiz.data.di.NetModule
import com.example.printfulquiiz.presentation.user.UsersActivity
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(
    includes = [
        CommonModule::class,
        DataModule::class,
        NetModule::class
    ]
)
abstract class ApplicationModule {

    @Binds
    internal abstract fun appContext(app: Application): Context

    @ContributesAndroidInjector(modules = [UsersModule::class])
    internal abstract fun mainNavActivity(): UsersActivity

}