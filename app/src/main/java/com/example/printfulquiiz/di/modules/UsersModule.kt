package com.example.printfulquiiz.di.modules
import androidx.lifecycle.ViewModel
import com.example.printfulquiiz.presentation.user.UsersViewModel
import dagger.Binds
import dagger.Module

@Module
abstract class UsersModule {

    @Binds
    internal abstract fun provideUsersViewModel(viewModel: UsersViewModel): ViewModel
}