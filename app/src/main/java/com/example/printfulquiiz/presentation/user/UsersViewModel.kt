package com.example.printfulquiiz.presentation.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.printfulquiiz.domain.model.UserDetails
import com.example.printfulquiiz.domain.usecases.CloseSocketUseCase
import com.example.printfulquiiz.domain.usecases.GetUsersCoordinatesUseCase
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class UsersViewModel @Inject constructor(
    private val getUsersCoordinatesUseCase: GetUsersCoordinatesUseCase,
    private val closeSocketUseCase: CloseSocketUseCase
) : ViewModel() {

    private val _viewState = MutableLiveData<UserDetails>()

    val viewData: LiveData<UserDetails>
        get() = _viewState

    fun getUsers() {
        viewModelScope.launch {
            getUsersCoordinatesUseCase.invoke()
                .collect {
                    // adding delay else for some reason it doesn't post all the values to the view
                    delay(10)
                    _viewState.postValue(it)
                }
        }
    }

    override fun onCleared() {
        viewModelScope.launch { closeSocketUseCase.invoke() }
        super.onCleared()
    }
}