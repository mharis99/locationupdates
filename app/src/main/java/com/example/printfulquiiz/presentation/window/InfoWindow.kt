package com.example.printfulquiiz.presentation.window

import android.content.Context
import android.location.Geocoder
import android.view.LayoutInflater
import android.view.View
import com.bumptech.glide.Glide
import com.example.printfulquiiz.databinding.UserInfoWindowBinding
import com.example.printfulquiiz.domain.model.UserDetails
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import java.util.*

class InfoWindow(private val context: Context) : GoogleMap.InfoWindowAdapter {

    private var binding: UserInfoWindowBinding =
        UserInfoWindowBinding.inflate(LayoutInflater.from(context))
    var geocoder: Geocoder = Geocoder(context, Locale.getDefault())

    private fun initWindowData(marker: Marker) {
        val userDetails = marker.tag as UserDetails

        binding.userName.text = userDetails.name
        binding.userAddress.text = geocoder.getFromLocation(
            marker.position.latitude,
            marker.position.longitude,
            1
        )[0].getAddressLine(0)
        Glide.with(context).load(userDetails.imageUrl).into(binding.userImage)
    }

    override fun getInfoContents(marker: Marker): View {
        initWindowData(marker)
        return binding.root
    }

    override fun getInfoWindow(marker: Marker): View? {
        initWindowData(marker)
        return binding.root
    }
}