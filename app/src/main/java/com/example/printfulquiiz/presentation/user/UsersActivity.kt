package com.example.printfulquiiz.presentation.user

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.printfulquiiz.R
import com.example.printfulquiiz.databinding.ActivityUsersBinding
import com.example.printfulquiiz.domain.model.UserDetails
import com.example.printfulquiiz.presentation.window.InfoWindow
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import dagger.android.AndroidInjection
import java.util.*
import javax.inject.Inject


class UsersActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private lateinit var mMap: GoogleMap
    private var markersMap: WeakHashMap<Int, Marker> = WeakHashMap<Int, Marker>()

    @Inject
    lateinit var viewModel: UsersViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(ActivityUsersBinding.inflate(layoutInflater).root)
        initMap()
    }

    private fun initMap() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun observeLiveData() {
        viewModel.viewData.observe(this, { findOrAddMarker(it) })
    }

    private fun findOrAddMarker(userDetails: UserDetails?) {

        userDetails?.run {
            val location = LatLng(
                userDetails.coordinates.latitude,
                userDetails.coordinates.longitude
            )
            if (markersMap[userDetails.id] == null) { // add marker if value not found in hashmap
                markersMap[userDetails.id] = mMap
                    .apply {
                        animateCamera( // animate to location on map
                            CameraUpdateFactory.newLatLngZoom(
                                location,
                                16f
                            )
                        )
                    }
                    .addMarker(MarkerOptions().position(location)) // return marker to set in hashmap
                    .apply { // set userdetails in tag of marker and update icon
                        tag = userDetails
                        setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_user))
                    }

            } else { //update marker
                markersMap[userDetails.id]?.run {
                    position = location
                    if (this.isInfoWindowShown) { showInfoWindow() } // show info window to dynamically update the address
                }
            }
        }
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setInfoWindowAdapter(InfoWindow(this))
        viewModel.getUsers()
        observeLiveData()
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        marker.showInfoWindow()
        return marker.isInfoWindowShown
    }
}