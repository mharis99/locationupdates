package com.example.printfulquiiz

import com.example.printfulquiiz.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class UsersApp : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<UsersApp> {
        return DaggerAppComponent.builder()
            .app(this)
            .build()
    }
}