package com.example.printfulquiiz.data.repository

import com.example.printfulquiiz.data.cache.UsersCache
import com.example.printfulquiiz.data.client.TcpClientImpl
import com.example.printfulquiiz.domain.contract.UsersData
import com.example.printfulquiiz.domain.model.UserDetails
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
        private val usersCache: UsersCache,
        private val tcpClient: TcpClientImpl
) : UsersData {

    @ExperimentalCoroutinesApi
    override suspend fun getUsersCoordinates(email: String): Flow<String> = channelFlow {
        tcpClient.connectSocket(email) { GlobalScope.launch { it?.run { send(it) } } }
        awaitClose()
    }

    override suspend fun saveUsersDetails(userDetails: List<UserDetails>) {
        usersCache.saveUsersDetails(userDetails)
    }

    override suspend fun getUserDetailsById(userID: Int): UserDetails? {
        return usersCache.getUserDetails(userID)
    }

    override suspend fun terminateSocket() {
        tcpClient.closeSocket()
    }
}