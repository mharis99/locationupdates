package com.example.printfulquiiz.data.cache

import com.example.printfulquiiz.domain.model.UserDetails


interface UsersCache {
    suspend fun saveUsersDetails(userDetails: List<UserDetails>)
    suspend fun getUserDetails(userId: Int): UserDetails?
    suspend fun getAllUsers(): List<UserDetails>?
}