package com.example.printfulquiiz.data.client

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.*
import java.net.InetAddress
import java.net.Socket
import javax.inject.Inject

class TcpClientImpl @Inject constructor() : TcpClient {

    private var mRun = false
    private lateinit var mBufferOut: PrintWriter
    private lateinit var mBufferIn: BufferedReader

    override suspend fun connectSocket(email: String, listener: (String?) -> Unit) {
        GlobalScope.launch {
            mRun = true
            try {
                val serverAddress = InetAddress.getByName(SERVER_IP)
                val socket = Socket(serverAddress, SERVER_PORT)
                try {
                    mBufferOut =
                            PrintWriter(BufferedWriter(OutputStreamWriter(socket.getOutputStream())), true)
                    mBufferOut.println("$AUTHORIZE_COMMAND $email")
                    mBufferIn = BufferedReader(InputStreamReader(socket.getInputStream()))
                    while (mRun) { listener(mBufferIn.readLine()) }
                } catch (e: Exception) { e.printStackTrace() }
                finally { socket.close() }
            } catch (e: Exception) { e.printStackTrace() }
        }
    }

    override fun closeSocket() {
        stopClient()
    }

    private fun stopClient() {
        mRun = false
        mBufferOut.flush()
        mBufferOut.close()
    }

    companion object {
        private const val AUTHORIZE_COMMAND = "AUTHORIZE" // authorize command
        private const val SERVER_IP = "ios-test.printful.lv" //server IP address
        private const val SERVER_PORT = 6111
    }
}