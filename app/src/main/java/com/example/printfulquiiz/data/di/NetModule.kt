package com.example.printfulquiiz.data.di

import com.example.printfulquiiz.data.client.TcpClientImpl
import com.example.printfulquiiz.data.client.TcpClient
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class NetModule {
    @Binds
    @Singleton
    internal abstract fun tcpClient(tcpClientImpl: TcpClientImpl): TcpClient
}