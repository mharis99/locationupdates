package com.example.printfulquiiz.data.cache

import com.example.printfulquiiz.domain.model.UserDetails
import javax.inject.Inject

class UsersCacheImpl @Inject constructor() : UsersCache {

    private lateinit var userDetails: List<UserDetails>

    override suspend fun saveUsersDetails(userDetails: List<UserDetails>) {
        this.userDetails = userDetails
    }

    override suspend fun getUserDetails(userId: Int): UserDetails? {
        return userDetails.find { it.id == userId }
    }

    override suspend fun getAllUsers(): List<UserDetails>? {
        return userDetails
    }
}