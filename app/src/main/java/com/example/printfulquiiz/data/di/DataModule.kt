package com.example.printfulquiiz.data.di

import com.example.printfulquiiz.data.cache.UsersCache
import com.example.printfulquiiz.data.cache.UsersCacheImpl
import com.example.printfulquiiz.data.repository.UserRepositoryImpl
import com.example.printfulquiiz.domain.contract.UsersData
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class DataModule {

    @Binds
    @Singleton
    internal abstract fun userDetails(userRepositoryImpl: UserRepositoryImpl): UsersData

    @Binds
    @Singleton
    internal abstract fun usersCache(usersCacheImpl: UsersCacheImpl): UsersCache
}