package com.example.printfulquiiz.data.client

interface TcpClient {
    suspend fun connectSocket(email: String, listener: (String?) -> Unit)
    fun closeSocket()
}