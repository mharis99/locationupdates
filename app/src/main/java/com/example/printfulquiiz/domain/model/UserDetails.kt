package com.example.printfulquiiz.domain.model

data class UserDetails(
    val id : Int,
    val name: String = "",
    val coordinates: Cooridinates,
    val imageUrl: String = ""
)