package com.example.printfulquiiz.domain.usecases

import com.example.printfulquiiz.domain.contract.UsersData
import com.example.printfulquiiz.domain.model.UserDetails
import javax.inject.Inject

class GetUserDetailsUseCase @Inject constructor(private val userData: UsersData) {
    suspend operator fun invoke(id: Int): UserDetails? {
        return userData.getUserDetailsById(id)
    }
}