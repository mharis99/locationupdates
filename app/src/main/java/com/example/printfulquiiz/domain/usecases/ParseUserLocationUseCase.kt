package com.example.printfulquiiz.domain.usecases

import com.example.printfulquiiz.domain.model.Cooridinates
import com.example.printfulquiiz.domain.model.UserDetails
import javax.inject.Inject

class ParseUserLocationUseCase @Inject constructor() {
    operator fun invoke(data: String): UserDetails {
        val user = data.replace(UPDATE_KEY, "", true).trim()
        val userCoordinates = user.split(",")
        return UserDetails(
            id = userCoordinates[0].toInt(),
            coordinates = Cooridinates(
                latitude = userCoordinates[1].toDouble(),
                longitude = userCoordinates[2].toDouble()
            )
        )
    }

    companion object{
        private const val UPDATE_KEY = "UPDATE"
    }
}