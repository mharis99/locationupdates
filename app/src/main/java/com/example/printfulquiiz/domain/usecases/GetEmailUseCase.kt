package com.example.printfulquiiz.domain.usecases

import javax.inject.Inject

class GetEmailUseCase @Inject constructor() {
    operator fun invoke(): String {
        return TEST_EMAIL_ID
    }

    companion object {
        private const val TEST_EMAIL_ID = "mharis99@gmail.com"
    }
}