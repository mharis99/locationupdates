package com.example.printfulquiiz.domain.usecases

import com.example.printfulquiiz.domain.contract.UsersData
import com.example.printfulquiiz.domain.model.UserDetails
import javax.inject.Inject

class SaveUsersDetailsUSeCase @Inject constructor(
    private val userData: UsersData,
    private val parseUsersUseCase: ParseUsersUseCase
) {
    suspend operator fun invoke(data: String): List<UserDetails> {
        val usersList = parseUsersUseCase.invoke(data)
        userData.saveUsersDetails(usersList)
        return usersList
    }
}