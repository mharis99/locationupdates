package com.example.printfulquiiz.domain.usecases

import com.example.printfulquiiz.domain.contract.UsersData
import com.example.printfulquiiz.domain.model.UserDetails
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetUsersCoordinatesUseCase @Inject constructor(
    private val usersData: UsersData,
    private val saveUsersDetailsUSeCase: SaveUsersDetailsUSeCase,
    private val parseUserLocationUseCase: ParseUserLocationUseCase,
    private val getEmailUseCase: GetEmailUseCase
) {
    suspend operator fun invoke(): Flow<UserDetails> = flow {
        usersData.getUsersCoordinates(getEmailUseCase.invoke()).collect {
            if (it.contains(USER_LIST_KEY, true)) {
                val users = saveUsersDetailsUSeCase.invoke(it)
                users.forEach { user -> emit(user) }
            } else {
                emit(parseUserLocationUseCase(it))
            }
        }
    }

    companion object {
        private const val USER_LIST_KEY = "USERLIST"
    }
}