package com.example.printfulquiiz.domain.usecases

import com.example.printfulquiiz.domain.contract.UsersData
import javax.inject.Inject

class CloseSocketUseCase @Inject constructor(
    private val usersData: UsersData,
) {
    suspend operator fun invoke() {
        usersData.terminateSocket()
    }
}