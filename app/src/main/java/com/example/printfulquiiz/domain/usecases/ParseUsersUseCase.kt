package com.example.printfulquiiz.domain.usecases
import com.example.printfulquiiz.domain.model.Cooridinates
import com.example.printfulquiiz.domain.model.UserDetails
import javax.inject.Inject

class ParseUsersUseCase @Inject constructor() {
    operator fun invoke(data: String): MutableList<UserDetails> {
        val usersList: MutableList<UserDetails> = mutableListOf()
        val users = data.replace("USERLIST", "", true).trim().dropLast(1)
        val list = users.split(";")
        list.forEach {
            val userDetails = it.split(",")
            usersList.add(
                UserDetails(
                    id = userDetails[0].toInt(),
                    name = userDetails[1],
                    imageUrl = userDetails[2],
                    coordinates = Cooridinates(
                        latitude = userDetails[3].toDouble(),
                        longitude = userDetails[4].toDouble()
                    )
                )
            )
        }
        return usersList
    }
}