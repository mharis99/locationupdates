package com.example.printfulquiiz.domain.model

data class Cooridinates(
    val latitude: Double,
    val longitude: Double
)