package com.example.printfulquiiz.domain.contract

import com.example.printfulquiiz.domain.model.UserDetails
import kotlinx.coroutines.flow.Flow

interface UsersData {
    suspend fun getUsersCoordinates(email: String): Flow<String>

    suspend fun saveUsersDetails(userDetails: List<UserDetails>)

    suspend fun getUserDetailsById(userID: Int): UserDetails?

    suspend fun terminateSocket()
}