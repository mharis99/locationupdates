# Location Updates Application


* This sample app connects to socket and start receiving the location updated of the user.
* It updates the user location on the map in real time.
* The code is based on Clean Architecture
* In presentation layer it uses MVVM
